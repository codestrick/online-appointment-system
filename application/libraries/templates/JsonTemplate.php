<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}
/**
 * Json template
 */
require_once 'Template.php';

/**
 * Json Template for encoding the PHP array into JSON serialized strings
 */
class JsonTemplate extends Template
{
    /**
     * JsonTemplate constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param       $content
     * @param array $data
     *
     * @return mixed|string
     */
    public function render($content, array $data = array())
    {
        $content = json_encode($data);

        return $content;
    }
}