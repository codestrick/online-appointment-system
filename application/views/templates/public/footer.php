<!-- footer -->
<div class="footer_top_agile_w3ls">
    <div class="container">
        <div class="col-md-5 footer_grid">
            <h3>About Us</h3>
            <p>
                <b>New Clinic</b> was established in 2002 by Dr. Prathap C Reddy.
                It has a robust presence across the healthcare ecosystem,
                Pharmacies, Primary Care & Diagnostic Clinics.
            </p>

        </div>
        <div class="col-md-3 footer_grid">
            <h3>Contact Info</h3>
            <ul class="address">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i>Kolkata, West Bengal 700136</li>
                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@newclinic.com">info@newclinic.com</a></li>
                <li><i class="fa fa-phone" aria-hidden="true"></i>+91 1230456789</li>
            </ul>
        </div>
        <div class="col-md-4 footer_grid">
            <h3>Social Media Contact Info</h3>
            <ul class="address">
                <li><i class="fa fa-facebook" aria-hidden="true"></i><a href="#">Facebook</a></li>
                <li><i class="fa fa-twitter" aria-hidden="true"></i><a href="#">Twitter</a></li>
                <li><i class="fa fa-youtube" aria-hidden="true"></i><a href="#">YouTube</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
<div class="footer_wthree_agile">
    <p>© 2018 New Clinic. All rights reserved | Design by <a href="#">CodeScript</a></p>
</div>
<!-- //footer -->