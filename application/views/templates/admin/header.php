<?php $admin_session = get_current_session('', 'admin'); ?>

<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu hidden-md-up float-xs-left">
                    <a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url('admin/dashboard'); ?>" class="navbar-brand nav-link text-center logo-href" style="margin-left: 3rem!important">
                        <span class="header-logo" style="text-align: center;color: white!important;"><i class="logo-class fa fa-stethoscope"></i> <strong class="logo-text shown">New Clinic</strong></span>
                    </a>
                </li>
                <li class="nav-item hidden-md-up float-xs-right">
                    <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container">
                        <i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content container-fluid">
            <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                <ul class="nav navbar-nav">
<!--                    <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5"> </i></a></li>-->
                    <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-xs-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                            <span class="avatar avatar-online">
                                <img src="<?php echo base_url(); ?>assets/images/user_icon.png" alt="avatar"><i></i></span>
                            <span class="user-name"><?php echo !empty($admin_session) ? $admin_session['name'] : ''; ?></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="<?php echo base_url('admin/profile'); ?>" class="dropdown-item"><i class="icon-head"></i> My Profile</a>
                            <a href="<?php echo base_url('admin/change-password'); ?>" class="dropdown-item"><i class="icon-clipboard2"></i> Change Password</a>
                            <div class="dropdown-divider"></div>
                            <a href="<?php echo base_url('admin/logout'); ?>" class="dropdown-item"><i class="icon-power3"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>