<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="keywords" content="New Clinic">
    <meta name="author" content="CodeScript">

    <title><?php echo ! empty($page_title) ? $page_title . ' | Admin | New Clinic' : 'New Clinic'; ?></title>

    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/images/stethoscope.png">

    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
    </script>

    <!-- BEGIN VENDOR CSS-->
    <?php echo $template_css; ?>
    <!-- END VENDOR CSS-->

</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

<?php echo $template_header; ?>

<!-- main menu-->
<?php echo $template_sidebar; ?>
<!-- / main menu-->

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            <?php echo $template_content; ?>

        </div>
    </div>
</div>

<!--footer-->
<?php echo $template_footer; ?>
<!--footer-->

<?php echo $template_js; ?>
</body>
</html>
