<!-- about -->
<div class="agile-about w3ls-section">
    <!-- about-bottom -->
    <div class="agileits-about-btm">
        <h3 class="heading-agileinfo">Welcome To Our Clinic!<span>We offer extensive medical procedures to outbound and inbound patients.</span></h3>
        <div class="container">
            <div class="w3-flex">
                <div class="col-md-4 col-sm-4 ab1 agileits-about-grid1">
                    <h4 class="agileinfo-head">Receive & Organize your Health Information</h4>
                    <p>Ask New Clinic Personal Health Record automatically downloads your test results from Apollo Hospitals. You can also use it to maintain a record of your medical conditions.</p>

                </div>
                <div class="col-md-4 col-sm-4 ab1 agileits-about-grid2">

                    <h4 class="agileinfo-head">Track & Monitor Your Health</h4>
                    <p>Ask New Clinic Personal Health Record makes it easy to join and stay on a wellness program - lose weight or manage a chronic condition easily.</p>

                 </div>
                <div class="col-md-4 col-sm-4 ab1 agileits-about-grid3">

                    <h4 class="agileinfo-head">Safe & Secure</h4>
                    <p>Ask New Clinic Personal Health Record stores all your data in a secure environment and gives you complete control over who accesses your information.</p>
                     </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //about-bottom -->
</div>
<!-- emergency -->
<div class="emergency_cases_w3ls">
    <div class="emergency_cases_bt">
        <div class="container">
            <div class="emergency_cases_top">
                <div class="col-md-6 emergency_cases_w3ls_left">
                    <h4>Opening Hours</h4>
                    <h6>Monday - Saturday&nbsp;<span class="eme">10.00 - 8.00</span></h6>
                    <h6>Sunday&nbsp;<span class="eme">11.00 - 6.00</span></h6>
                </div>
                <div class="col-md-6 emergency_cases_w3ls_right">
                    <h4>Emergency Cases</h4>
                    <h5><i class="fa fa-phone" aria-hidden="true"></i>1230456789</h5>
                    <p>Your treatment plan is designed for steady progress, with every phase promptly implemented.</p>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- //emergency -->
<!-- services -->
<div class="services">
    <div class="container">
        <h3 class="heading-agileinfo">Therapies & Treatments<span>We offer extensive medical procedures to outbound and inbound patients.</span></h3>

        <div class="services-top-grids">
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i>
                    <h4>Anxiety</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-user-md" aria-hidden="true"></i>
                    <h4>Executive Coaching</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-wheelchair-alt" aria-hidden="true"></i>
                    <h4>Depression</h4>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="services-bottom-grids">
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-medkit" aria-hidden="true"></i>
                    <h4>Relationships</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-hospital-o" aria-hidden="true"></i>
                    <h4>Stress Management</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-ambulance" aria-hidden="true"></i>
                    <h4>Support Group</h4>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //services -->
<hr>
<!--/icons-->
<div class="banner-bottom">
    <div class="container">
        <div class="tittle_head_w3layouts">
            <h3 class="heading-agileinfo">WHY CHOOSE US?</h3>
        </div>
        <div class="inner_sec_info_agileits_w3">
            <div class="col-md-4 grid_info" style="height:460px">
                <div class="icon_info">
                    <img src="<?php echo base_url(); ?>assets/public/images/g4.jpg" alt=" " class="img-responsive">
                    <h5>Trusted</h5>
                    <p>Care meets cure. Choose from a pool of highly skilled, renowned and credible doctors.</p>
                </div>
            </div>
            <div class="col-md-4 grid_info" style="height:460px">
                <div class="icon_info">
                    <img src="<?php echo base_url(); ?>assets/public/images/g7.jpg" alt=" " class="img-responsive">
                    <h5>Healthcare Simplified!</h5>
                    <p>   Health solutions at the click of a button. Yes, it’s that simple.</p>
                </div>
            </div>
            <div class="col-md-4 grid_info" style="height:460px">
                <div class="icon_info">
                    <img src="<?php echo base_url(); ?>assets/public/images/g6.jpg" alt=" " class="img-responsive">
                    <h5>Uncompromised Privacy!</h5>
                    <p>With strong encryption techniques and processes, your data is always safe and confidential.</p>
                </div>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--//icons-->