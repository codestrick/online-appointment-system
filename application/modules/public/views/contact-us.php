<div class="banner-bottom">
    <div class="container">
        <div class="inner_sec_info_agileits_w3">
            <h2 class="heading-agileinfo">Contact Us<span>We offer extensive medical procedures to outbound and inbound patients.</span></h2>
            <div class="contact-form">

                <!--jquery validation error container-->
                <div id="errorContainer" class="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p class="text-left">Please correct the following errors and try again:</p>
                    <ul class="text-left" style="list-style: disc!important;"></ul>
                </div>

                <?php
                if ( ! empty($flash_message))
                {
                    ?>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?php echo $flash_message; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                if ( ! empty($validation_err))
                {
                    ?>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="alert alert-danger alert-dismissible fade in mb-2">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?php echo $validation_err; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <form id="save_form" method="post" action="">
                    <div class="left_form">
                        <div>
                            <span><label>Name</label></span>
                            <span><input name="userName" id="userName" type="text" class="textbox" required data-msg-required="Name Required"></span>
                        </div>
                        <div>
                            <span><label>E-mail</label></span>
                            <span><input name="userEmail" id="userEmail" type="text" class="textbox" required data-msg-required="E-mail Required"></span>
                        </div>
                        <div>
                            <span><label>Mobile</label></span>
                            <span><input name="userPhone" id="userPhone" type="text" class="textbox" required data-msg-required="Phone Required"></span>
                        </div>
                    </div>
                    <div class="right_form">
                        <div>
                            <span><label>Message</label></span>
                            <span><textarea name="userMessage" id="userMessage" required data-msg-required="Message Required"> </textarea></span>
                        </div>
                        <div>
                            <span><input type="submit" value="Submit" class="myButton"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>


    </div>
</div>
<!-- /map -->
<div class="map_w3layouts_agile">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29460.564909039953!2d88.44872765754069!3d22.6325042029053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39f89ff55d3c47d3%3A0x9b5422b4bf682cb5!2sNew+Clinic!5e0!3m2!1sen!2sin!4v1524157209141"
            style="border:0">
    </iframe>
</div>
<!-- //map -->
