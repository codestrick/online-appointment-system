<div class="w3ls-banner">
    <div class="heading">
        <h1>Patient Registration<hr></h1>
    </div>
    <div class="container_1">
        <div class="heading">
            <h2>Please Enter Patients Details</h2>
            <p></p>
            <?php
            if ( ! empty($flash_message))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $flash_message; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            if ( ! empty($validation_err))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert alert-danger alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $validation_err; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!--jquery validation error container-->
            <div id="errorContainer" class="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p class="text-left">Please correct the following errors and try again:</p>
                <ul class="text-left" style="list-style: disc!important;"></ul>
            </div>
        </div>
        <div class="agile-form">
            <form action="" method="post" id="save_form">
                <ul class="field-list">
                    <li>
                        <label class="form-label">
                            Full Name
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <input type="text" name="patient_name" id="patient_name" placeholder="Enter Patients Name" required data-msg-required="Patients Name Required">
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Date of Birth
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <input type="text" name="date_of_birth" id="date_of_birth" maxlength="10" placeholder="Enter Date of Birth" required data-msg-required="Date of Birth Required">
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Gender
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <select class="form-dropdown" name="gender" id="gender" required data-msg-required="Gender Required">
                                <option value="">Select Gender</option>
                                <option value="Male"> Male</option>
                                <option value="Female"> Female</option>
                                <option value="Transgender"> Transgender</option>
                            </select>
                        </div>
                    <li>
                    <li>
                        <label class="form-label">
                            Mobile Number
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <input type="text" name="mobile_number" id="mobile_number" maxlength="10" placeholder="Mobile Number" required data-msg-required="Mobile Number Required">
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            E-Mail Address
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input">
                            <input type="email" name="email" id="email" placeholder="myname@example.com" required data-msg-required="E-mail Required">
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Address
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input add">
								<span class="form-sub-label">
									<input type="text" name="address" id="address" placeholder=" " required data-msg-required="Address Required">
									<label class="form-sub-label1"> Address </label>
								</span>
                            <span class="form-sub-label">
									<input type="text" name="state" id="state" placeholder=" " required data-msg-required="State Required">
									<label class="form-sub-label1"> State / Province </label>
								</span>
                            <span class="form-sub-label">
									<input type="text" name="city" id="city" placeholder=" " required data-msg-required="City Required">
									<label class="form-sub-label1"> City </label>
								</span>
                            <span class="form-sub-label">
									<input type="text" name="zipcode" id="zipcode" placeholder=" " maxlength="6" required data-msg-required="Postal / Zipcode Required">
									<label class="form-sub-label1"> Postal / Zip Code </label>
								</span>
                        </div>
                    </li>
                    <li>
                        <label class="form-label">
                            Medical History
                            <span class="form-required"> * </span>
                        </label>
                        <div class="form-input add">
                            <span class="form-sub-label">
									<input type="text" name="height" id="height" placeholder=" " required data-msg-required="Height Required">
									<label class="form-sub-label1"> Height </label>
                            </span>
                            <span class="form-sub-label">
									<input type="text" name="weight" id="weight" placeholder=" " required  data-msg-required="Weight Required">
									<label class="form-sub-label1"> Weight</label>
                            </span>
                            <span class="form-sub-label">
                                <select class="form-dropdown" name="blood_group" id="blood_group" style="width: 221px" required data-msg-required="Blood Group Required">
                                    <option value="">Select Blood Group</option>
                                    <?php
                                    if ( ! empty($blood_groups))
                                    {
                                        foreach ($blood_groups as $group_k => $group_v)
                                        {
                                            $selected = '';
                                            echo '<option value="' . $group_k . '"' . $selected . '>' . $group_v . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                                <label class="form-sub-label1"> Blood Group</label>
                            </span>
                            <span class="form-sub-label">
									<input type="text" name="blood_pressure" id="blood_pressure" placeholder=" " required data-msg-required="Blood Pressure Required">
									<label class="form-sub-label1"> Blood Pressure </label>
                            </span>
                            <span class="form-sub-label" style="top: 10px">
                                <select class="form-dropdown" name="past_diseases" id="past_diseases" style="width: 221px" required data-msg-required="Any Past Diseases Required">
                                    <option value="">Any Past Diseases?</option>
                                    <option value="Yes"> Yes</option>
                                    <option value="No"> No</option>
                                </select>
                                <label class="form-sub-label1"> Past Diseases</label>
                            </span>
                            <span class="form-sub-label" style="top: 10px">
                                <select class="form-dropdown" name="allergies" id="allergies" style="width: 221px" required data-msg-required="Any Allergies Required">
                                    <option value="">Any Allergies?</option>
                                    <option value="Yes"> Yes</option>
                                    <option value="No"> No</option>
                                </select>
                                <label class="form-sub-label1"> Allergies</label>
                            </span>
                        </div>
                    </li>
                </ul>
                <br>
                <input type="submit" value="Register">
            </form>
        </div>
    </div>
</div>