<div class="about">
    <div class="container">
        <h3 class="heading-agileinfo">What We Do<span>We offer extensive medical procedures to outbound and inbound patients.</span></h3>
        <div class="col-md-6 about-w3right">
            <img src="<?php echo base_url('assets/public/images/g6.jpg'); ?>" class="img-responsive" alt=""/>
        </div>
        <div class="col-md-6 about-w3left">
            <p class="text-justify">
                <b>New Clinic</b> was established in 2002 by Dr. Prathap C Reddy. It was India’s first corporate hospital,
                and is acclaimed for pioneering the private healthcare revolution in the country. Since then,
                <b>New Clinic</b> has risen to a position of leadership and has emerged as Asia’s foremost integrated healthcare
                services provider. It has a robust presence across the healthcare ecosystem,
                Pharmacies, Primary Care & Diagnostic Clinics.
            </p>
            <p class="text-justify">
                As a responsible corporate citizen, <b>New Clinic</b> takes the spirit of leadership
                well beyond business and it has embraced the responsibility of keeping India healthy. Recognizing that
                Non Communicable Diseases (NCDs) are the greatest threat to the nation, <b>New Clinic</b> is continuously
                educating its fellow Indians on personalized preventive healthcare as a key to wellness.
            </p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>