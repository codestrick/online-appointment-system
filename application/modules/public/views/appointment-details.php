<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url('assets/css/module/public/print.css') ?>"/>

<div class="w3ls-banner">
    <div class="heading">
        <h1>Appointment Details
            <hr>
        </h1>
    </div>
    <div class="container_1">
        <div class="heading">
            <h2><?php echo ( ! empty($patient_details) && ! empty($appointment_details)) ? 'PLEASE PRINT A COPY OF THIS APPOINTMENT' : ''; ?></h2>
            <hr class="heading-hr">
            <?php
            if ( ! empty($flash_message))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $flash_message; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            if ( ! empty($validation_err))
            {
                ?>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="alert alert-danger alert-dismissible fade in mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $validation_err; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <!--jquery validation error container-->
            <div id="errorContainer" class="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p class="text-left">Please correct the following errors and try again:</p>
                <ul class="text-left" style="list-style: disc!important;"></ul>
            </div>
        </div>

        <div class="row">
            <table class="table" style="border: 1px solid gainsboro">
                <tbody>
                <?php

                $style = 'display:none';

                if ( ! empty($patient_details) && ! empty($appointment_details))
                {
                    $style = '';

                    echo '<tr>';
                    echo '<th class="text-right" width="300px">Appointment No</th>';
                    echo '<th width="10px">:</th>';
                    echo '<th>' . $appointment_details['appnt_no'] . '</th>';
                    echo '</tr>';

                    echo '<tr>';
                    echo '<th class="text-right" width="300px">Patient Name</th>';
                    echo '<th width="10px">:</th>';
                    echo '<th>' . $patient_details['name'] . '</th>';
                    echo '</tr>';

                    echo '<tr>';
                    echo '<th class="text-right" width="300px">Doctor Name</th>';
                    echo '<th width="10px">:</th>';
                    echo '<th>' . $appointment_details['doctor_name'] . '</th>';
                    echo '</tr>';

                    echo '<tr>';
                    echo '<th class="text-right" width="300px">Date of Appointment</th>';
                    echo '<th width="10px">:</th>';
                    echo '<th>' . date('d-m-Y', strtotime($appointment_details['booking_date'])) . '</th>';
                    echo '</tr>';

                    echo '<tr>';
                    echo '<th class="text-right" width="300px">Time of Appointment</th>';
                    echo '<th width="10px">:</th>';
                    echo '<th>' . $appointment_details['booking_time'] . '</th>';
                    echo '</tr>';
                }
                else
                {
                    echo '<li class="text-center">No Data found</li>';
                }
                ?>
                </tbody>
            </table>
            <div class="text-center printButtonDiv">
                <hr>
                <button type="submit" class="printButton btn btn-warning" onclick="window.print()" style="<?php echo $style; ?>">
                    <i class="fa fa-print"></i>
                    PRINT
                </button>
                <a class="btn btn-warning text-capitalize another-appointment" href="<?php echo base_url('appointment'); ?>">
                    <i class="fa fa-calendar-check-o"></i>&nbsp;
                    MAKE ANOTHER APPOINTMENT
                </a>
            </div>
        </div>
    </div>
</div>