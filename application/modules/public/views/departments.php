<div class="services">
    <div class="container">
        <h3 class="heading-agileinfo">Departments<span>We offer extensive medical procedures to outbound and inbound patients.</span></h3>
        <?php
        if ( ! empty($departments))
        {
            $counter = 1;
            foreach ($departments as $key => $value)
            {
                if ($counter == 1)
                {
                    echo '<div class="services-top-grids">';
                }

                echo '<div class="col-md-4">';
                echo '<div class="grid1" style="height: 300px;">';
                echo '<i class="fa fa-user-md" aria-hidden="true"></i>';
                echo '<h4>' . $value['dept_name'] . '</h4>';
                echo '<p>' . $value['dept_desc'] . '</p>';
                echo '</div>';
                echo '</div>';

                if(($key + 1) == count($departments))
                {
                    $counter = 0;
                    echo '<div class="clearfix"></div>';
                    echo '</div>';
                }
                else
                {
                    if ($counter == 3)
                    {
                        $counter = 0;
                        echo '<div class="clearfix"></div>';
                        echo '</div>';
                    }
                }

                $counter = $counter + 1;
            }
        }
        else
        {
            echo '<h4 class="text-center">No Departments Found!</h4>';
        }
        ?>
    </div>
</div>