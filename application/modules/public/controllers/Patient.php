<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends My_Controller
{
    public $autoload = array(
        'libraries' => array('form_validation'),
        'model'     => array('App_model'),
    );

    /**
     * Patient constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Patient Login
     */
    public function login()
    {
        check_patient_session('patient');

        $this->form_validation->set_rules('mobile_number', 'Mobile Number', "trim|required");
        $this->form_validation->set_rules('date_of_birth', 'Date of Birth', "trim|required");

        if ( ! $this->form_validation->run())
        {
            $dataArray['page_title']  = 'Patient Login';
            $dataArray['header_type'] = 'header2';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $dataArray['css_local'] = ['bootstrap-datepicker'];
            $dataArray['js_local']  = [
                'jquery.validate',
                'validate-additional-methods',
                'alphanumeric',
                'bootstrap-datepicker',
                'patient',
            ];

            $this->load->view('patient-login', $dataArray);
        }
        else
        {
            $dob = $this->input->post('date_of_birth');

            $params = [
                'dob'    => date('Y-m-d', strtotime($dob)),
                'mobile' => $this->input->post('mobile_number')
            ];

            $patient_details = $this->App_model->get_detail_by('patient', $params);

            if ( ! empty($patient_details))
            {
                $this->session->set_userdata('new_clinic', array(
                    'patient' => array(
                        'id'           => $patient_details['id'],
                        'patient_id'   => $patient_details['patient_id'],
                        'name'         => $patient_details['name'],
                        'email'        => $patient_details['email'],
                        'mobile'       => $patient_details['mobile'],
                        'is_logged_in' => TRUE
                    )
                ));

                $this->session->set_flashdata('flash_message', 'You logged in successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect(base_url('appointment'));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Error, Please provide valid login credential.');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('patient/login'));
            }
        }
    }

    /**
     * Patient Registration
     */
    public function register()
    {
        check_patient_session('patient');

        $this->form_validation->set_rules('patient_name', 'Patient Name', "trim|required");
        $this->form_validation->set_rules('date_of_birth', 'Date of Birth', "trim|required");
        $this->form_validation->set_rules('gender', 'Gender', "trim|required");
        $this->form_validation->set_rules('email', 'E-mail', "trim|required");
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', "trim|required");
        $this->form_validation->set_rules('address', 'Address', "trim|required|max_length[255]");
        $this->form_validation->set_rules('state', 'State', "trim|required");
        $this->form_validation->set_rules('city', 'City', "trim|required");
        $this->form_validation->set_rules('zipcode', 'Pin Code/ Zip Code', "trim|required");
        $this->form_validation->set_rules('height', 'Height', "trim|required");
        $this->form_validation->set_rules('weight', 'Weight', "trim|required");
        $this->form_validation->set_rules('blood_group', 'Blood Group', "trim|required");
        $this->form_validation->set_rules('blood_pressure', 'Blood Pressure', "trim|required");
        $this->form_validation->set_rules('past_diseases', 'Past Diseases', "trim|required");
        $this->form_validation->set_rules('allergies', 'Allergies', "trim|required");

        if ( ! $this->form_validation->run())
        {
            $dataArray['blood_groups'] = get_blood_groups();

            $dataArray['page_title']  = 'Patient Registration';
            $dataArray['header_type'] = 'header2';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $dataArray['css_local'] = ['bootstrap-datepicker'];
            $dataArray['js_local']  = [
                'jquery.validate',
                'validate-additional-methods',
                'alphanumeric',
                'bootstrap-datepicker',
                'patient',
            ];

            $this->load->view('patient-register', $dataArray);
        }
        else
        {
            $dob = $this->input->post('date_of_birth');

            $params = [
                'name'           => $this->input->post('patient_name'),
                'dob'            => date('Y-m-d', strtotime($dob)),
                'gender'         => $this->input->post('gender'),
                'email'          => $this->input->post('email'),
                'mobile'         => $this->input->post('mobile_number'),
                'address'        => $this->input->post('address'),
                'state'          => $this->input->post('state'),
                'city'           => $this->input->post('city'),
                'pin_code'       => $this->input->post('zipcode'),
                'height'         => $this->input->post('height'),
                'weight'         => $this->input->post('weight'),
                'blood_group'    => $this->input->post('blood_group'),
                'blood_pressure' => $this->input->post('blood_pressure'),
                'past_disease'   => $this->input->post('past_diseases'),
                'allergies'      => $this->input->post('allergies'),
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ];

            $new_patient_id = $this->App_model->save_details('patient', $params);

            if ( ! empty($new_patient_id))
            {
                $patient_id_status = $this->App_model->save_details('patient', ['patient_id' => 'PT-00' . $new_patient_id], ['id' => $new_patient_id]);

                $this->session->set_flashdata('flash_message', 'Registration completed successfully. Please login to book an appointment');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect(base_url('patient/login'));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('patient/registration'));
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('new_clinic');

        redirect(base_url('patient/login'));
    }
}