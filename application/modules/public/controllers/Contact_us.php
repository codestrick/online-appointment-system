<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends My_Controller
{
    public $autoload = array(
        'libraries' => array('form_validation'),
        'model'     => array('App_model'),
    );

    /**
     * Contact_us constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $this->form_validation->set_rules('userName', 'Name', "trim|required");
        $this->form_validation->set_rules('userEmail', 'E-mail', "trim|required");
        $this->form_validation->set_rules('userPhone', 'Phone Number', "trim|required");
        $this->form_validation->set_rules('userMessage', 'Message', "trim|required");

        if ( ! $this->form_validation->run())
        {
            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $dataArray['css_local'] = [];
            $dataArray['js_local']  = [
                'jquery.validate',
                'validate-additional-methods',
                'alphanumeric',
            ];

            $dataArray['page_title']  = 'Contact Us';
            $dataArray['header_type'] = 'header2';

            $dataArray['css_local'] = [];
            $dataArray['js_local']  = [];

            $this->load->view('contact-us', $dataArray);
        }
        else
        {
            $params = [
                'name'    => $this->input->post('userName'),
                'email'   => $this->input->post('userEmail'),
                'phone'   => $this->input->post('userPhone'),
                'message' => $this->input->post('userMessage'),
            ];

            $new_contact_id = $this->App_model->save_details('contact_us', $params);

            if ( ! empty($new_contact_id))
            {
                $this->session->set_flashdata('flash_message', 'Your message send successfully. We will contact you shortly.');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect(base_url('contact-us'));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('contact-us'));
            }
        }
    }
}