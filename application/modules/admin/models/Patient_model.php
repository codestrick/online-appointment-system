<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_model extends MY_Model
{
    public $tbl_name = 'patient';

    /**
     * Patient_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $params
     * @param null  $id
     *
     * @return null
     */
    function save_patient($params = [], $id = NULL)
    {
        $return = NULL;

        if ( ! empty($id))
        {
            $this->db->update($this->tbl_name, $params, ['id' => $id]);
            $return = $id;
        }
        else
        {
            $this->db->insert($this->tbl_name, $params);
            $return = $this->db->insert_id();
        }

        return $return;
    }

    /**
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_patient_detail_by($params = [], $return_type = 'row_array')
    {
        $result = NULL;

        if ($return_type == 'result_array')
        {
            $result = $this->db->get_where($this->tbl_name, $params)->result_array();
        }
        else
        {
            $result = $this->db->get_where($this->tbl_name, $params)->row_array();
        }

        return $result;
    }

    /**
     * @param array $pagingParams
     *
     * @return array|null
     */
    function get_all_patient_datatable($pagingParams = array())
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 1', FALSE);
        $this->db->select($this->select_db_cols);

        if ( ! empty($pagingParams['order_by']))
        {
            if (empty($pagingParams['order_direction']))
            {
                $pagingParams['order_direction'] = '';
            }

            switch ($pagingParams['order_by'])
            {
                default:
                    $this->db->order_by($pagingParams['order_by'],
                        $pagingParams['order_direction']);
                    break;
            }
        }

        $search = empty($pagingParams['search']) ? array() : $pagingParams['search'];
        if ( ! empty($search))
        {
            $date = date('Y-m-d H:i:s', strtotime($search));

            $this->db->where("($this->list_search_key LIKE '%$search%' ESCAPE '!' 
            OR $this->list_search_key1 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key2 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key3 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key4 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key5 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key6 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key7 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key8 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key9 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key10 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key11 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key12 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key13 LIKE '%$search%' ESCAPE '!'
            OR $this->list_search_key14 LIKE '$date' ESCAPE '!'
            OR $this->list_search_key15 LIKE '$date' ESCAPE '!'
            )");
        }

        $return = $this->getWithCount($this->tbl_name, $pagingParams['records_per_page'], $pagingParams['offset']);

        return $return;
    }

    /**
     * @param $params
     *
     * @return array
     */
    function delete_patient($params)
    {
        $status = $this->db->delete($this->tbl_name, $params);

        if ($status == TRUE)
        {
            $statusMessage = 'Patient record successfully deleted.';
        }
        else
        {
            $statusMessage = 'Patient record delete error.';
        }

        return [
            'status' => $status,
            'msg'    => $statusMessage,
        ];
    }
}
