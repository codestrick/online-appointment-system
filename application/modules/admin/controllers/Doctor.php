<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Doctor_model', 'Department_model'),
    );

    /**
     * Doctor constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add, Edit Doctor
     *
     * @param null $doctor_id
     */
    public function add($doctor_id = NULL)
    {
        $this->form_validation->set_rules('dept_id', 'Department Name', "trim|required");
        $this->form_validation->set_rules('name', 'Doctor Name', "trim|required");
        $this->form_validation->set_rules('email', 'Doctor E-mail', "trim|required|valid_email");
        $this->form_validation->set_rules('mobile', 'Doctor Mobile Number', "trim|required");
        $this->form_validation->set_rules('address', 'Doctor Address', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($doctor_id))
            {
                $doctor_details = $this->Doctor_model->get_doctor_detail_by(['id' => $doctor_id]);

                if ( ! empty($doctor_details))
                {
                    $dataArray = array(
                        'dept_id' => $doctor_details['dept_id'],
                        'name'    => $doctor_details['name'],
                        'email'   => $doctor_details['email'],
                        'mobile'  => $doctor_details['mobile'],
                        'address' => $doctor_details['address']
                    );

                    $dataArray['form_action'] = 'Edit Doctor';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/doctor/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Doctor';
            }

            $dataArray['departments'] = $this->Department_model->get_department_detail_by([], 'result_array');

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title']     = 'Doctor';
            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('doctor/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'dept_id' => $this->input->post('dept_id'),
                'name'    => $this->input->post('name'),
                'email'   => $this->input->post('email'),
                'mobile'  => $this->input->post('mobile'),
                'address' => $this->input->post('address'),
            );

            if ( ! empty($doctor_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_doctor_id = $this->Doctor_model->save_doctor($params, $doctor_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_doctor_id = $this->Doctor_model->save_doctor($params);
            }

            if ( ! empty($new_doctor_id))
            {
                $updated_doctor_id = $this->Doctor_model->save_doctor(['doc_id' => 'DOC-00' . $new_doctor_id], $new_doctor_id);

                $this->session->set_flashdata('flash_message', (empty($doctor_id)) ? 'Doctor created successfully' : 'Doctor updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/doctor/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/doctor/list');
            }
        }
    }

    /**
     * Doctor View
     */
    public function index()
    {
        $listing_headers = 'doctor_listing_headers';

        $data['source']          = site_url('admin/doctor/listDoctor_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Doctor';
        $data['page_title']      = 'Doctor';

        $dataArray = $this->_table_listing($data);

        $this->load->view('doctor/index', $dataArray);
    }

    /**
     * Doctor DataTable JSON
     */
    public function listDoctor_Json()
    {
        $listing_headers = 'doctor_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Doctor_model->tbl_name = 'doctor';

        $this->Doctor_model->join_tbl_name = 'department';
        $this->Doctor_model->join_cond     = "{$this->Doctor_model->tbl_name}.dept_id = {$this->Doctor_model->join_tbl_name}.id";

        $this->Doctor_model->select_db_cols = "{$this->Doctor_model->tbl_name}.id,
                                                 {$this->Doctor_model->tbl_name}.doc_id, 
                                                 {$this->Doctor_model->tbl_name}.dept_id, 
                                                 {$this->Doctor_model->join_tbl_name}.dept_name, 
                                                 {$this->Doctor_model->tbl_name}.name, 
                                                 {$this->Doctor_model->tbl_name}.email, 
                                                 {$this->Doctor_model->tbl_name}.mobile, 
                                                 {$this->Doctor_model->tbl_name}.address, 
                                                 {$this->Doctor_model->tbl_name}.created_at, 
                                                 {$this->Doctor_model->tbl_name}.updated_at";

        $this->Doctor_model->list_search_key  = "{$this->Doctor_model->tbl_name}.name";
        $this->Doctor_model->list_search_key1 = "{$this->Doctor_model->tbl_name}.email";
        $this->Doctor_model->list_search_key2 = "{$this->Doctor_model->tbl_name}.mobile";
        $this->Doctor_model->list_search_key3 = "{$this->Doctor_model->tbl_name}.address";
        $this->Doctor_model->list_search_key4 = "{$this->Doctor_model->tbl_name}.created_at";
        $this->Doctor_model->list_search_key5 = "{$this->Doctor_model->tbl_name}.updated_at";
        $this->Doctor_model->list_search_key6 = "{$this->Doctor_model->join_tbl_name}.dept_name";

        $resultdata = $this->Doctor_model->get_all_doctor_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Doctor
     *
     * @param $doctor_id
     */
    public function delete($doctor_id)
    {
        $res = $this->Doctor_model->delete_doctor(['id' => $doctor_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/doctor/list');
    }
}