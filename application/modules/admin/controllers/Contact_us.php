<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('report_library'),
        'model'     => array('Contact_us_model'),
    );

    /**
     * Contact_us constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Contact_us View
     */
    public function index()
    {
        $listing_headers = 'contact_us_listing_headers';

        $data['source']          = site_url('admin/Contact_us/listContactUs_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Contact Us';
        $data['page_title']      = 'Contact Us';

        $dataArray = $this->_table_listing($data);

        $this->load->view('contact-us/index', $dataArray);
    }

    /**
     * Contact_us DataTable JSON
     */
    public function listContactUs_Json()
    {
        $listing_headers = 'contact_us_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Contact_us_model->tbl_name       = 'contact_us';
        $this->Contact_us_model->select_db_cols = "id, name, email, phone, message, created_at, updated_at";

        $this->Contact_us_model->list_search_key  = 'name';
        $this->Contact_us_model->list_search_key1 = 'email';
        $this->Contact_us_model->list_search_key2 = 'phone';
        $this->Contact_us_model->list_search_key3 = 'message';

        $resultdata = $this->Contact_us_model->get_all_contact_us_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Contact_us
     *
     * @param $department_id
     */
    public function delete($department_id)
    {
        $res = $this->Contact_us_model->delete_contact_us(['id' => $department_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect(base_url('admin/contact-us/list'));
    }
}