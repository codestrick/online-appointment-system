<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation'),
        'model'     => array('Admin_model'),
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
    {
        $dataArray['page_title']  = 'Dashboard';
        $dataArray['form_action'] = 'Dashboard';

        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $this->load->view('dashboard', $dataArray);
    }

    public function profile()
    {
        $session = get_current_session('', 'admin');

        $this->form_validation->set_rules('name', 'Name', "trim|required");
        $this->form_validation->set_rules('email', 'E-mail', "trim|required|valid_email");
        $this->form_validation->set_rules('mobile', 'Mobile Number', "trim|required|max_length[10]|numeric");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($session))
            {
                $dataArray = array(
                    'name'   => $session['name'],
                    'email'  => $session['email'],
                    'mobile' => $session['mobile'],
                );
            }

            $dataArray['page_title']  = 'Profile';
            $dataArray['form_action'] = 'Profile';

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('profile', $dataArray);
        }
        else
        {
            $params = array(
                'name'       => $this->input->post('name'),
                'email'      => $this->input->post('email'),
                'mobile'     => $this->input->post('mobile'),
                'updated_at' => date("Y-m-d H:i:s"),
            );

            $new_admin_id = $this->Admin_model->save_admin($params, $session['id']);

            if ( ! empty($new_admin_id))
            {
                $this->update_cureent_session($new_admin_id);

                $this->session->set_flashdata('flash_message', 'Profile updated successfully.');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect(base_url('admin/profile'));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/profile'));
            }
        }
    }

    private function update_cureent_session($new_admin_id = NULL)
    {
        if ( ! empty($new_admin_id))
        {
            $admin_cred = $this->Admin_model->get_admin_detail_by(['id' => $new_admin_id]);

            if ( ! empty($admin_cred))
            {
                $session = $this->session->userdata('user_auth');

                if ( ! empty($session))
                {
                    $session['name']   = $admin_cred['name'];
                    $session['email']  = $admin_cred['email'];
                    $session['mobile'] = $admin_cred['mobile'];

                    $this->session->set_userdata('user_auth', $session);
                }
            }
        }
    }

    public function change_password()
    {
        $session = get_current_session('', 'admin');

        $this->form_validation->set_rules('old_pass', 'Old Password', "trim|required");
        $this->form_validation->set_rules('new_pass', 'New Password', "trim|required");
        $this->form_validation->set_rules('re_enter_pass', 'Re Enter Password', "trim|required");

        if ( ! $this->form_validation->run())
        {
            $dataArray['page_title']  = 'Change Password';
            $dataArray['form_action'] = 'Change Password';

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('change-password', $dataArray);
        }
        else
        {
            $old_pass      = $this->input->post('old_pass');
            $new_pass      = $this->input->post('new_pass');
            $re_enter_pass = $this->input->post('re_enter_pass');

            if ($new_pass == $re_enter_pass)
            {
                $admin_cred = $this->Admin_model->get_admin_detail_by(['password' => md5($old_pass)]);

                if ( ! empty($admin_cred))
                {
                    $params = array(
                        'password'   => md5($new_pass),
                        'updated_at' => date("Y-m-d H:i:s"),
                    );

                    $new_admin_id = $this->Admin_model->save_admin($params, $session['id']);

                    if ( ! empty($new_admin_id))
                    {
                        $this->update_cureent_session($new_admin_id);

                        $this->session->set_flashdata('flash_message', 'Password changed successfully.');
                        $this->session->set_flashdata('flash_message_status', TRUE);

                        redirect(base_url('admin/change-password'));
                    }
                    else
                    {
                        $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                        $this->session->set_flashdata('flash_message_status', FALSE);

                        redirect(base_url('admin/change-password'));
                    }
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Old Password!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect(base_url('admin/change-password'));
                }
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'New Password & Re Enter Password are not equal.');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/change-password'));
            }
        }
    }
}