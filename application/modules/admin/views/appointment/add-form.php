<?php
if ( ! empty($flash_message))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
    <?php
}
if ( ! empty($validation_err))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert alert-danger alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<!--jquery validation error container-->
<div id="errorContainer" class="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p>Please correct the following errors and try again:</p>
    <ul></ul>
</div>

<section class="card">
    <div class="card-header">
        <h2 class="card-title"><?php echo ! empty($form_action) ? $form_action : ''; ?></h2>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                <li>
                    <a href="<?php echo base_url('admin/appointment/list'); ?>" class="btn btn-primary btn-block btn-min-width mr-1 mb-1" role="button">
                        <i class="fa fa-list" style="color: white;"></i>&nbsp;
                        List Appointment
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form id="save_form" action="<?php echo current_url(); ?>" method="post">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Patient Name</strong></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="patient_name" id="patient_name" autocomplete="off"
                                   value="<?php echo ! empty($patient_name) ? $patient_name : $this->input->post('patient_name'); ?>"
                                   placeholder="Enter Patient Name" disabled required data-msg-required="Patient Name Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Department Name</strong></label>
                        <div class="col-lg-4">
                            <select id="dept_id" name="dept_id" class="form-control" disabled required data-msg-required="Department Name Required">
                                <option value="">Select Department</option>
                                <?php
                                if ( ! empty($departments))
                                {
                                    foreach ($departments as $dept)
                                    {
                                        $selected = ($dept['id'] == (! empty($dept_id) ? $dept_id : $this->input->post('dept_id')) ? 'selected' : '');
                                        echo '<option value="' . $dept['id'] . '"' . $selected . '>' . $dept['dept_name'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Doctor Name</strong></label>
                        <div class="col-lg-4">
                            <select id="doct_id" name="doct_id" class="form-control" disabled required data-msg-required="Department Name Required">
                                <option value="">Select Doctor</option>
                                <?php
                                if ( ! empty($doctors))
                                {
                                    foreach ($doctors as $dept)
                                    {
                                        $selected = ($dept['id'] == (! empty($doct_id) ? $doct_id : $this->input->post('doct_id')) ? 'selected' : '');
                                        echo '<option value="' . $dept['id'] . '"' . $selected . '>' . $dept['name'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Appointment Date</strong></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="booking_date" id="booking_date" autocomplete="off"
                                   value="<?php echo ! empty($booking_date) ? $booking_date : $this->input->post('booking_date'); ?>"
                                   placeholder="Enter Appointment Date" required data-msg-required="Appointment Date Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Appointment Time</strong></label>
                        <div class="col-lg-4">
                            <select name="booking_time" id="booking_time" class="form-control" required data-msg-required="Appointment Time Required">
                                <option value="">Select Time of Appointment</option>
                                <?php
                                if ( ! empty($time_slots))
                                {
                                    foreach ($time_slots as $slot_k => $slot_v)
                                    {
                                        $selected = ($slot_k == (! empty($booking_time) ? $booking_time : $this->input->post('booking_time')) ? 'selected' : '');
                                        echo '<option value="' . $slot_k . '"' . $selected . '>' . $slot_v . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>