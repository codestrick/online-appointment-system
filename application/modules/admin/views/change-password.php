<?php
if ( ! empty($flash_message))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<section class="card">
    <div class="card-header">
        <h2 class="card-title"><?php echo ! empty($form_action) ? $form_action : ''; ?></h2>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form id="save_form" action="<?php echo current_url(); ?>" method="post">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Old Password</strong></label>
                        <div class="col-lg-4">
                            <input type="password" class="form-control" name="old_pass" id="old_pass" autocomplete="off"
                                   value="<?php echo ! empty($old_pass) ? $old_pass : $this->input->post('old_pass'); ?>"
                                   placeholder="Enter Old Password" required data-msg-required="Old Password Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>New Password</strong></label>
                        <div class="col-lg-4">
                            <input type="password" class="form-control" name="new_pass" id="new_pass" autocomplete="off"
                                   value="<?php echo ! empty($new_pass) ? $new_pass : $this->input->post('new_pass'); ?>"
                                   placeholder="Enter New Password" required data-msg-required="New Password Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Re Enter Password</strong></label>
                        <div class="col-lg-4">
                            <input type="password" class="form-control" name="re_enter_pass" id="re_enter_pass" autocomplete="off"
                                   value="<?php echo ! empty($re_enter_pass) ? $re_enter_pass : $this->input->post('re_enter_pass'); ?>"
                                   placeholder="Enter Re Enter Password" required data-msg-required="Re Enter Password Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>