<?php
if ( ! empty($flash_message))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<section class="card">
    <div class="card-header">
        <h2 class="card-title"><?php echo ! empty($form_action) ? $form_action : ''; ?></h2>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form id="save_form" action="<?php echo current_url(); ?>" method="post">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Name</strong></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" id="name" autocomplete="off"
                                   value="<?php echo ! empty($name) ? $name : $this->input->post('name'); ?>"
                                   placeholder="Enter Name" required data-msg-required="Name Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>E-mail</strong></label>
                        <div class="col-lg-4">
                            <input type="email" class="form-control" name="email" id="email" autocomplete="off"
                                   value="<?php echo ! empty($email) ? $email : $this->input->post('email'); ?>"
                                   placeholder="Enter E-mail" required data-msg-required="E-mail Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"><strong>Mobile Number</strong></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="mobile" id="mobile" autocomplete="off"
                                   value="<?php echo ! empty($mobile) ? $mobile : $this->input->post('mobile'); ?>"
                                   placeholder="Enter Mobile Number" required data-msg-required="Mobile Number Required"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-4">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>