<?php
if ( ! empty($flash_message))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<section class="card bg-cyan">
    <div class="card-body">
        <div class="card-block">
            <div class="card-text">
                <h3 class="white" style="text-align: center; text-transform: uppercase">Welcome in the new clinic admin dashboard section!</h3>
            </div>
        </div>
    </div>
</section>

<div class="row">
    <div class="col-xl-3 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="pink"><a href="<?php echo base_url('admin/department/list'); ?>" class="pink">Department</a></h3>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-grid2 pink font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="teal"><a href="<?php echo base_url('admin/doctor/list'); ?>" class="teal">Doctor</a></h3>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-user-md teal font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="deep-orange"><a href="<?php echo base_url('admin/patient/list'); ?>" class="deep-orange">Patient</a></h3>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-bed deep-orange font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="cyan"><a href="<?php echo base_url('admin/appointment/list'); ?>" class="cyan">Appointment</a></h3>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-calendar-check-o cyan font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>