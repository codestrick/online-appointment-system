<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends MY_Model
{
    public $tbl_name = 'admin';

    /**
     * Auth_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null   $tbl_name
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_detail_by($tbl_name = NULL, $params = [], $return_type = 'row_array')
    {
        $this->tbl_name = ! empty($tbl_name) ? $tbl_name : $this->tbl_name;

        $result = NULL;

        if ( ! empty($params))
        {
            if ($return_type == 'result_array')
            {
                $result = $this->db->get_where($this->tbl_name, $params)->result_array();
            }
            else
            {
                $result = $this->db->get_where($this->tbl_name, $params)->row_array();
            }
        }
        else
        {
            if ($return_type == 'result_array')
            {
                $result = $this->db->get($this->tbl_name)->result_array();
            }
            else
            {
                $result = $this->db->get($this->tbl_name)->row_array();
            }
        }

        return $result;
    }
}
