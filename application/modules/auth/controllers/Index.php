<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation'),
        'model'     => array('Auth_model'),
    );

    /**
     * Admin Login View & Validate
     */
    public function login()
    {
        $this->form_validation->set_rules('uemail', 'Username', 'required|max_length[255]');
        $this->form_validation->set_rules('upass', 'Password', 'required');

        if ($this->form_validation->run())
        {
            $data = array(
                'uemail' => $this->input->post('uemail'),
                'upass'  => $this->input->post('upass'),
            );

            $admin_cred = $this->Auth_model->get_detail_by('', ['user_name' => $data['uemail']]);

            if (($data['uemail'] == $admin_cred['user_name']) && ($admin_cred['password'] == md5($data['upass'])))
            {
                $this->session->set_userdata('user_auth', array(
                    'id'        => $admin_cred['id'],
                    'name'      => $admin_cred['name'],
                    'email'     => $admin_cred['email'],
                    'mobile'    => $admin_cred['mobile'],
                    'connected' => TRUE,
                ));

                $this->session->set_flashdata('flash_message', 'You have been successfully logged in.');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect(base_url('admin/dashboard'));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Error, Please provide valid login credential.');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/login'));
            }
        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('login', $data);
        }
    }

    /**
     * Logout Admin
     */
    public function logout()
    {
        $this->session->unset_userdata('user_auth');

        $this->session->set_flashdata('flash_message', 'You have been successfully logged out.');
        $this->session->set_flashdata('flash_message_status', TRUE);

        redirect(base_url('admin/login'));
    }

    /**
     * Forgot Password View & Validation
     */
    public function forgot_password()
    {
        if ($this->form_validation->run())
        {

        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('recover-password', $data);
        }
    }

    /**
     * Reset Password View & Validation
     */
    public function reset_password()
    {
        if ($this->form_validation->run())
        {

        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('reset-password', $data);
        }
    }
}