<?php
if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

/**
 * Description of MY_form_validation_lang
 */

$lang['alpha_dash_mod']             = "Please enter a valid username (Allowed chars [A-Z] [a-z] [0-9] [_] [.], Should start with an alphabet, 6 to 20 characters long, Special chars cannot be placed together or start or end of the username.)";
$lang['is_equal']                   = "Value mismatch for %s";
$lang['is_unique_modified']         = "%s value already exists, please try another one";
$lang['valid_date']                 = "Invalid format provided for %s, required format YYYY-MM-DD";
$lang['is_pos']                     = "%s field is Negative";
$lang['password_validation_check']  = "%s should be 8 Char AlphaNumeric and 1 Char in Caps";
$lang['is_valid_amount_payments']   = "%s should not greater than the total unpaid amount";
$lang['is_valid_container_receipt'] = "%s should not greater than the due container";
$lang['is_valid_discount']          = "Input %s is greater than the Payment Amount";
$lang['valid_date_time']            = "Invalid format provided for %s, required format YYYY-MM-DD HH:MM:SS";