$(document).ready(function () {

    $("#appointment_date").datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        toggleActive: true,
        startDate: '-d'
    });

    $('#appointment_date').on("change", function () {

        var dept_id = $('#dept_id').val();
        var appointment_date = $('#appointment_date').val();

        $.ajax({
            url: base_url + 'appointment/get-health-check-slot-ajax',
            data: {
                dept_id: dept_id,
                appointment_date: appointment_date
            },
            method: 'POST',
            dataType: 'json'
        }).done(function (response) {

            if (response.status == 'true')
            {
                var toAppend = '';

                $('#appointment_time').empty();

                toAppend += '<option value="">' + 'Select Time of Health Check' + '</option>';

                $.each(response.time_slot, function (k, v) {

                    if (typeof response.appointments[k] != "undefined")
                    {
                        toAppend += '<option value="' + k + '" disabled>' + v + '</option>';
                    }
                    else
                    {
                        toAppend += '<option value="' + k + '">' + v + '</option>';
                    }
                });

                $('#appointment_time').append(toAppend);
            }
            else
            {
                var toAppend = '';

                $('#appointment_time').empty();

                toAppend += '<option value="">' + 'Select Time of Health Check' + '</option>';

                $.each(response.time_slot, function (k, v) {
                    toAppend += '<option value="' + k + '">' + v + '</option>';
                });

                $('#appointment_time').append(toAppend);
            }
        });
    });

    $("#save_form").validate({
        ignore: [],
        errorContainer: $('#errorContainer'),
        errorLabelContainer: $('#errorContainer ul'),
        wrapper: 'li',
        onfocusout: false,
        highlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').addClass(errorClass);
            }
            else
            {
                $(element).addClass(errorClass);
            }
        },
        unhighlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').removeClass(errorClass);
            }
            else
            {
                $(element).removeClass(errorClass);
            }
        }
    });
});