$(document).ready(function () {

    $('#mobile_number, #zipcode, #height, #weight').numeric();

    $("#date_of_birth").datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        toggleActive: true,
        endDate: '-d'
    });

    $("#appointment_date").datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        toggleActive: true,
        startDate: '-d'
    });

    $('#dept_id').on("change", function () {

        var dept_id = $(this).val();

        $.ajax({
            url: base_url + 'appointment/get-doctor-by-dept-ajax',
            data: {
                dept_id: dept_id
            },
            method: 'POST',
            dataType: 'json'
        }).done(function (response) {

            var toAppend = '';

            $('#doctor_id').empty();

            if (response.status == 'true')
            {
                toAppend += '<option value="">' + 'Select Doctor' + '</option>';

                $.each(response.data, function (k, v) {
                    toAppend += '<option value=' + v.id + '>' + v.name + '</option>';
                });

                $('#doctor_id').append(toAppend);
            }
            else
            {
                toAppend = '<option value="">' + 'No Doctor Found' + '</option>';

                $('#doctor_id').append(toAppend);
            }
        });
    });

    $('#appointment_date').on("change", function () {

        var dept_id = $('#dept_id').val();
        var doctor_id = $('#doctor_id').val();
        var appointment_date = $('#appointment_date').val();

        $.ajax({
            url: base_url + 'appointment/get-appointment-slot-ajax',
            data: {
                dept_id: dept_id,
                doctor_id: doctor_id,
                appointment_date: appointment_date
            },
            method: 'POST',
            dataType: 'json'
        }).done(function (response) {

            if (response.status == 'true')
            {
                var toAppend = '';

                $('#appointment_time').empty();

                toAppend += '<option value="">' + 'Select Time of Appointment' + '</option>';

                $.each(response.time_slot, function (k, v) {

                    if (typeof response.appointments[k] != "undefined")
                    {
                        toAppend += '<option value="' + k + '" disabled>' + v + '</option>';
                    }
                    else
                    {
                        toAppend += '<option value="' + k + '">' + v + '</option>';
                    }
                });

                $('#appointment_time').append(toAppend);
            }
            else
            {
                var toAppend = '';

                $('#appointment_time').empty();

                toAppend += '<option value="">' + 'Select Time of Appointment' + '</option>';

                $.each(response.time_slot, function (k, v) {
                    toAppend += '<option value="' + k + '">' + v + '</option>';
                });

                $('#appointment_time').append(toAppend);
            }
        });
    });

    $("#save_form").validate({
        ignore: [],
        errorContainer: $('#errorContainer'),
        errorLabelContainer: $('#errorContainer ul'),
        wrapper: 'li',
        onfocusout: false,
        highlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').addClass(errorClass);
            }
            else
            {
                $(element).addClass(errorClass);
            }
        },
        unhighlight: function (element, errorClass) {
            if ($(element).hasClass('select-2'))
            {
                $(element).next('.select2-container').removeClass(errorClass);
            }
            else
            {
                $(element).removeClass(errorClass);
            }
        }
    });
});