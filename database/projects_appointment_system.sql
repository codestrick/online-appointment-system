-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2018 at 07:05 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projects_appointment_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `mobile`, `user_name`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Koushik', 'admin@admin.com', '0123456789', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2018-04-14 08:37:59', '2018-04-14 05:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `appnt_no` varchar(255) DEFAULT NULL,
  `dept_id` bigint(20) DEFAULT NULL,
  `doct_id` bigint(20) DEFAULT NULL,
  `patient_id` bigint(20) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `booking_time` varchar(255) DEFAULT NULL,
  `appnt_status` enum('Pending','Cancelled','Complete') NOT NULL DEFAULT 'Pending',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `appnt_no`, `dept_id`, `doct_id`, `patient_id`, `booking_date`, `booking_time`, `appnt_status`, `created_at`, `updated_at`) VALUES
(1, 'APPN-001', 1, 1, 1, '2018-04-12', '01:30 PM', 'Pending', '2018-04-10 08:26:15', '2018-04-23 10:07:37'),
(2, 'APPN-002', 1, 1, 1, '2018-04-19', '01:45 PM', 'Pending', '2018-04-19 16:44:03', '2018-04-23 10:07:39'),
(3, 'APPN-003', 10, NULL, 1, '2018-04-22', '10:15 AM', 'Pending', '2018-04-22 15:42:06', '2018-04-23 10:07:41'),
(4, 'APPN-004', 10, NULL, 1, '2018-04-22', '11:30 AM', 'Pending', '2018-04-22 17:16:35', '2018-04-23 10:07:53'),
(5, 'APPN-005', 10, NULL, 1, '2018-04-22', '11:30 AM', 'Pending', '2018-04-23 10:08:10', '2018-04-23 10:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `message` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Koushik Samanta', 'koushiksamanta@gmail.com', '+661-71-5853216', 'Qui dolore sunt aperiam sunt aut laboris voluptas modi earum', '2018-04-12 23:35:49', '2018-04-20 00:03:14');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dept_id` varchar(255) DEFAULT NULL,
  `dept_type` enum('Pathology','Others') DEFAULT 'Others',
  `dept_name` varchar(255) DEFAULT NULL,
  `dept_desc` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `dept_id`, `dept_type`, `dept_name`, `dept_desc`, `created_at`, `updated_at`) VALUES
(1, 'DEPT-001', 'Others', 'Ear nose and throat (ENT)', 'The ENT department provides care for patients with a variety of problems.', '2018-04-19 17:35:27', '2018-04-22 18:55:52'),
(2, 'DEPT-002', 'Others', 'Gynaecology', 'These departments investigate and treat problems of the female urinary tract and reproductive organs, such as endometritis, infertility and incontinence.', '2018-04-19 17:35:57', '2018-04-22 18:55:54'),
(3, 'DEPT-003', 'Others', 'Neurology', 'This unit deals with disorders of the nervous system, including the brain and spinal cord. It\'s run by doctors who specialise in this area (neurologists) and their staff.', '2018-04-19 17:36:21', '2018-04-22 18:55:56'),
(4, 'DEPT-004', 'Others', 'Nutrition and dietetics', 'Trained dieticians and nutritionists provide specialist advice on diet for hospital wards and outpatient clinics, forming part of a multidisciplinary team.', '2018-04-19 17:36:36', '2018-04-22 18:55:59'),
(5, 'DEPT-005', 'Others', 'Oncology', 'This department provides radiotherapy and a full range of chemotherapy treatments for cancerous tumours and blood disorders.', '2018-04-19 17:36:56', '2018-04-22 18:56:01'),
(6, 'DEPT-006', NULL, 'Orthopaedics', 'Orthopaedic departments treat problems that affect your musculoskeletal system. That\'s your muscles, joints, bones, ligaments, tendons and nerves.', '2018-04-19 17:37:15', '2018-04-19 17:42:28'),
(7, 'DEPT-007', 'Others', 'Physiotherapy', 'Physiotherapists promote body healing, for example after surgery, through therapies such as exercise and manipulation.', '2018-04-19 17:37:30', '2018-04-22 18:56:06'),
(8, 'DEPT-008', 'Others', 'CARDIOLOGY', 'Cardiology is a branch of medicine dealing with disorders of the heart as well as parts of the circulatory system.', '2018-04-19 17:50:18', '2018-04-22 18:56:08'),
(9, 'DEPT-009', 'Others', 'DENTAL', 'Dentistry is a branch of medicine that consists of the study, diagnosis, prevention, and treatment of diseases, disorders, and conditions of the oral cavity, commonly in the dentition.', '2018-04-19 17:52:20', '2018-04-22 18:56:13'),
(10, 'DEPT-0010', 'Pathology', 'X-RAY', 'X-rays are a type of radiation called electromagnetic waves. X-ray imaging creates pictures of the inside of your body.', '2018-04-22 15:41:55', '2018-04-23 06:18:51'),
(11, 'DEPT-0011', 'Pathology', 'USG', 'An ultrasound / SONOGRAPHY is a procedure that uses high-frequency sound waves to scan a the internal organs of the body woman’s abdomen and pelvic cavity.', '2018-04-23 06:17:05', '2018-04-23 06:18:33'),
(12, 'DEPT-0012', 'Pathology', 'ECG', 'Electrocardiography (ECG or EKG) is the process of recording the electrical activity of the heart over a period of time using electrodes placed on the skin.', '2018-04-23 06:17:44', '2018-04-23 09:47:45');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doc_id` varchar(255) DEFAULT NULL,
  `dept_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `address` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `doc_id`, `dept_id`, `name`, `email`, `mobile`, `address`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Destiny Christensen', 'koushiksamanta@gmail.com', '1234567890', 'Esse cumque natus doloribus rerum facilis nihil quo natus', '2018-04-08 08:32:36', '2018-04-08 14:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varbinary(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `pin_code` varchar(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `blood_group` varchar(255) DEFAULT NULL,
  `blood_pressure` varchar(255) DEFAULT NULL,
  `past_disease` varchar(255) DEFAULT NULL,
  `allergies` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `patient_id`, `name`, `gender`, `email`, `mobile`, `dob`, `address`, `state`, `city`, `pin_code`, `height`, `weight`, `blood_group`, `blood_pressure`, `past_disease`, `allergies`, `created_at`, `updated_at`) VALUES
(1, 'PT-001', 'Abra Bush', 'Transgender', 'koushik@gmail.com', 0x38373638373535343933, '1995-04-28', 'In enim et', 'Obcaecati', 'Maxime ', '67316', '45', '45', 'O-', 'Voluptatem', 'Yes', 'Yes', '2018-04-10 07:10:30', '2018-04-11 11:02:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
